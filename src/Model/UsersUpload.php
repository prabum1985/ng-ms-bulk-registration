<?php

namespace Travelodge\MS\BulkRegistrationBundle\Model;

use Travelodge\MS\BulkRegistrationBundle\Validator\Constraints as CustomConstraint;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UsersUpload
 * @package Travelodge\MS\BulkRegistrationBundle\Model
 *
 * @CustomConstraint\UserRegistrationUploadConstraint()
 */
class UsersUpload
{
    /**
     * @var array
     * @Serializer\Type("array<Travelodge\MS\BulkRegistrationBundle\Model\UserUpload>")
     * @Assert\Valid()
     * @Assert\NotBlank(
     * )
     */
    private $users;

    /**
     * @return array
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param array $users
     */
    public function setUsers(array $users)
    {
        $this->users = $users;
    }
}
