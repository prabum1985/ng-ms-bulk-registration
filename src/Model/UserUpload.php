<?php

namespace Travelodge\MS\BulkRegistrationBundle\Model;

use Symfony\Component\Validator\Constraints as Assert;
use Travelodge\MS\BulkRegistrationBundle\Validator\Constraints as CustomConstraint;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class UserUpload
 * @package Travelodge\MS\BulkRegistrationBundle\Model
 */
class UserUpload
{
    /**
     * @var array
     * @Serializer\Type("array")
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    private $firstname;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    private $lastname;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     */
    private $email;

    /**
     * @var array
     * @Serializer\Type("array")
     * @Assert\NotBlank()
     */
    private $roles;

    /**
     * @var integer
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     */
    private $phone;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    private $address_line_1;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    private $address_line_2;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    private $address_line_3;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    private $address_line_4;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    private $postcode;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    private $country;

    /**
     *
     * @return  string
     */ 
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     *
     * @param  string  $firstname 
     *
     * @return  self
     */ 
    public function setFirstname(string $firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * 
     * @return  string
     */ 
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     *
     * @param  string  $lastname
     *
     * @return  self
     */ 
    public function setLastname(string $lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * 
     * @return  string
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     *
     * @param  string  $email
     *
     * @return  self
     */ 
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     *
     * @return  integer
     */ 
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     *
     * @param  integer  $phone 
     *
     * @return  self
     */ 
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     *
     * @return  string
     */ 
    public function getAddOne()
    {
        return $this->address_line_1;
    }

    /**
     *
     * @param  string  $address_line_1 
     *
     * @return  self
     */ 
    public function setAddOne(string $address_line_1)
    {
        $this->address_line_1 = $address_line_1;

        return $this;
    }

    /**
     *
     * @return  string
     */ 
    public function getAddTwo()
    {
        return $this->address_line_2;
    }

    /**
     *
     * @param  string  $address_line_2
     *
     * @return  self
     */ 
    public function setAddTwo(string $address_line_2)
    {
        $this->address_line_2 = $address_line_2;

        return $this;
    }

    /**
     *
     * @return  string
     */ 
    public function getAddThree()
    {
        return $this->address_line_3;
    }

    /**
     *
     * @param  string  $address_line_3 
     *
     * @return  self
     */ 
    public function setAddThree(string $address_line_3)
    {
        $this->address_line_3 = $address_line_3;

        return $this;
    }

    /**
     * @return  string
     */ 
    public function getAddFour()
    {
        return $this->address_line_4;
    }

    /**
     *
     * @param  string  $address_line_4
     *
     * @return  self
     */ 
    public function setAddFour(string $address_line_4)
    {
        $this->address_line_4 = $address_line_4;

        return $this;
    }

    /**
     *
     * @return  string
     */ 
    public function getPostCode()
    {
        return $this->postcode;
    }

    /**
     *
     * @param  string  $postcode 
     *
     * @return  self
     */ 
    public function setPostCode(string $postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     *
     * @return  string
     */ 
    public function getCountry()
    {
        return $this->country;
    }

    /**
     *
     * @param  string  $country
     *
     * @return  self
     */ 
    public function setCountry(string $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get the value of title
     *
     * @return  array
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @param  array  $title
     *
     * @return  self
     */ 
    public function setTitle(array $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of roles
     *
     * @return  array
     */ 
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set the value of roles
     *
     * @param  array  $roles
     *
     * @return  self
     */ 
    public function setRoles(array $roles)
    {
        $this->roles = $roles;

        return $this;
    }
}
