<?php

namespace Travelodge\MS\BulkRegistrationBundle\Validator;

use Travelodge\MS\BulkRegistrationBundle\Model\UsersUpload;
use App\Repository\UserRepository;
use App\Repository\MemberEmailRepository;
use App\Repository\CorporateMemberRepository;
use App\Repository\RolesRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use App\Enumeration\UserTitle;

/**
 * Class UserRegistrationUploadConstraintValidator
 * @package Travelodge\MS\BulkRegistrationBundle\Validator
 */
class UserRegistrationUploadConstraintValidator extends ConstraintValidator
{
    /** @var UserRepository */
    private $userRepository;

    /** @var MemberEmailRepository */
    private $emailRepository;

    /** @var CorporateMemberRepository */
    private $corporateRepository;

    /** @var RolesRepository */
    private $roleRepository;

    /**
     * UserRegistrationUploadConstraintValidator constructor.
     * @param UserRepository $userRepository
     * @param MemberEmailRepository $emailRepository
     * @param CorporateMemberRepository $corporateRepository
     * @param RolesRepository $roleRepository
     */
    public function __construct(UserRepository $userRepository,
    MemberEmailRepository $emailRepository,
    CorporateMemberRepository $corporateRepository,
    RolesRepository $roleRepository)
    {
        $this->userRepository = $userRepository;
        $this->emailRepository = $emailRepository;
        $this->corporateRepository = $corporateRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * @param UsersUpload $user
     * @param Constraint $constraint
     */
    public function validate($users, Constraint $constraint)
    {
        $lineNo = 1;
        $emails = [];
        foreach ($users->getUsers() as $users) {
            if ($this->userRepository->findBy(['username' => $users->getEmail()]) ||
                $this->emailRepository->findBy(['email' => $users->getEmail()]) ||
                $this->corporateRepository->findBy(['email' => $users->getEmail()]) ) {
                $this->context
                    ->buildViolation($constraint->emailMessage)
                    ->setParameters(["%email%" => $users->getEmail()])
                    ->setInvalidValue($users->getEmail())
                    ->atPath("users[" . $lineNo . "].email")
                    ->addViolation();
            }
            $emails[$lineNo] = $users->getEmail();
            foreach($users->getRoles() as $ind => $role)
            {
                if(!$this->roleRepository->validateRoles($role)) {
                    $this->context
                    ->buildViolation($constraint->invalidRole)
                    ->setParameters(["%role%" => $role])
                    ->setInvalidValue($role)
                    ->atPath("users[" . $lineNo . "].roles")
                    ->addViolation();
                }
            }
            
            $getTitle = new UserTitle();
            foreach($users->getTitle() as $ind => $title)
            {
                if($getTitle->getAvailableTitle($title) ==null) {
                    $this->context
                    ->buildViolation($constraint->invalidTitle)
                    ->setParameters(["%title%" => $title])
                    ->setInvalidValue($title)
                    ->atPath("users[" . $lineNo . "].title")
                    ->addViolation();
                }
            }
            $lineNo++;
        }

        $allDuplicateEmails = array_unique( array_diff_assoc( $emails, array_unique( $emails ) ) );
        if(count($allDuplicateEmails) > 0)
        {
            foreach($allDuplicateEmails as $index => $email) {
                $this->context
                ->buildViolation($constraint->emailDupMessage)
                ->setParameters(["%email%" => $email])
                ->setInvalidValue($users->getEmail())
                ->atPath("users[" . $index . "].email")
                ->addViolation();
            }
        }

    }
}
