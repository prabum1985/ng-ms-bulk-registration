<?php

namespace Travelodge\MS\BulkRegistrationBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UserRegistrationUploadConstraint extends Constraint
{
    public $emailMessage = '%email% Email Already Registered';

    public $emailDupMessage = '%email% Email is duplicated';

    public $invalidRole = '%role% Role mentioned is invalid';

    public $invalidTitle = '%title% Title mentioned is invalid';

    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'user_registration_upload_constraint_validator';
    }

    /**
     * @return array|string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
