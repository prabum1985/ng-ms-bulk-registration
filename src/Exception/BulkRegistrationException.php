<?php
namespace Travelodge\MS\BulkRegistrationBundle\Exception;

/**
 * Class BulkRegistrationException
 * @package Travelodge\MS\BulkRegistrationBundle\Exception
 */
class BulkRegistrationException extends \Exception
{

}