<?php
namespace Travelodge\MS\BulkRegistrationBundle\Controller;

use App\Entity\Member;
use App\Representation\Members;
use App\Entity\CorporateMember;
use App\Representation\CorporateMembers;
use App\Entity\User;
use App\Representation\Users;
use App\Exception\ApiProblemException;
use App\Model\ApiProblem;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Swagger\Annotations as SWG;
use Travelodge\MS\BulkRegistrationBundle\Model\UserUpload;
use Travelodge\MS\BulkRegistrationBundle\Model\UsersUpload;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


/**
 * Class BulkRegistrationController
 * @package Travelodge\MS\BulkRegistrationBundle\Controller
 */
class BulkRegistrationController extends FOSRestController
{
    /**
     * @Rest\Post(path="/user/register", name="bom_users_register")
     * 
     * @ParamConverter(
     *      "users",
     *      converter="fos_rest.request_body"
     * )
     * 
     * @SWG\Response(
     *     response=201,
     *     description="All Members Registered"
     * )
     * 
     */
    public function postRegistrationAction(UsersUpload $users, ConstraintViolationListInterface $violations)
    {

        if (count($violations)) {
            throw new ApiProblemException(
                new ApiProblem(Response::HTTP_BAD_REQUEST, $violations, ApiProblem::TYPE_VALIDATION_ERROR)
            );
        }
        $users = $this->get('ms_bulk_registration_service')->saveMember($users);
        $response = new JsonResponse(array('upload' => $users));
        return $response; 
    }

    /**
     * @Route("/members", name="api_members_detail", methods={"GET"})
     * 
     * @Rest\QueryParam(
     *      name="username",
     *      nullable=true,
     *      description="Username"
     * )
     * 
     * @Rest\QueryParam(
     *      name="page",
     *      nullable=false,
     *      description="Page"
     * )
     * 
     * @Rest\QueryParam(
     *      name="limit",
     *      nullable=false,
     *      description="Number of results per page"
     * )
     * 
     * @SWG\Response(
     *     response=200,
     *     description="Returns all the users"
     * )
     */
    public function getMembersAction(ParamFetcherInterface $paramFetcher)
    {
        $limit = (empty($paramFetcher->get('limit'))) ? 25 : $paramFetcher->get('limit');
        $page = (empty($paramFetcher->get('page'))) ? 1 : $paramFetcher->get('page');

        $memberQuery = $this->getDoctrine()->getRepository(Member::class)
                 ->getUsers();
                 
        if (count($memberQuery->getResult())) {
            /** @var Member $member */
            foreach ($memberQuery->getResult() as $member) {
                $results[] = $member;
            }
        }
        
        return new Members($results,  
        $this->getDoctrine()->getRepository(Member::class)->getCount(), 
        $limit, 
        $page);
    }

    /**
     * @Route("/business/members", name="api_business_members_detail", methods={"GET"})
     * 
     * @Rest\QueryParam(
     *      name="username",
     *      nullable=true,
     *      description="Username"
     * )
     * 
     * @Rest\QueryParam(
     *      name="page",
     *      nullable=false,
     *      description="Page"
     * )
     * 
     * @Rest\QueryParam(
     *      name="limit",
     *      nullable=false,
     *      description="Number of results per page"
     * )
     * 
     * @SWG\Response(
     *     response=200,
     *     description="Returns all the users"
     * )
     */
    public function getBusinessMembersAction(ParamFetcherInterface $paramFetcher)
    {
        $limit = (empty($paramFetcher->get('limit'))) ? 25 : $paramFetcher->get('limit');
        $page = (empty($paramFetcher->get('page'))) ? 1 : $paramFetcher->get('page');

        $memberQuery = $this->getDoctrine()->getRepository(CorporateMember::class)
                 ->getUsers();
                 
        if (count($memberQuery->getResult())) {
            /** @var Member $member */
            foreach ($memberQuery->getResult() as $member) {
                $results[] = $member;
            }
        }
        
        return new CorporateMembers($results,  
            $this->getDoctrine()->getRepository(CorporateMember::class)->getCount(), 
            $limit, 
            $page);
    }

    /**
     * @Route("/users", name="api_users_detail", methods={"GET"})
     * 
     * @Rest\QueryParam(
     *      name="order_by",
     *      description="Order by"
     * )
     *
     * @Rest\QueryParam(
     *      name="order_direction",
     *      default="ASC",
     *      description="Order direction (ascending or descending)"
     * )
     * 
     * @Rest\QueryParam(
     *      name="page",
     *      nullable=false,
     *      description="Page"
     * )
     * 
     * @Rest\QueryParam(
     *      name="limit",
     *      nullable=false,
     *      description="Number of results per page"
     * )
     * 
     * @SWG\Response(
     *     response=200,
     *     description="Returns all the users"
     * )
     */
    public function getUsersAction(ParamFetcherInterface $paramFetcher)
    {
        $limit = (empty($paramFetcher->get('limit'))) ? 25 : $paramFetcher->get('limit');
        $page = (empty($paramFetcher->get('page'))) ? 1 : $paramFetcher->get('page');

        $memberQuery = $this->getDoctrine()->getRepository(User::class)
                 ->getUsers();
        $results = [];        
        if (count($memberQuery->getResult())) {
            /** @var Member $member */
            foreach ($memberQuery->getResult() as $member) {
                $results[] = $member;
            }
        }
        
        return new Users($results,  
            $this->getDoctrine()->getRepository(User::class)->getCount(), 
            $limit, 
            $page);
    }

}