<?php
namespace Travelodge\MS\BulkRegistrationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class TravelodgeBulkRegistrationBundle
 * @package Travelodge\MS\BulkRegistrationBundle
 */
class TravelodgeBulkRegistrationBundle extends Bundle
{
}