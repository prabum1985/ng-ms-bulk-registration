<?php

namespace Travelodge\MS\BulkRegistrationBundle\Services;

use App\Entity\Address;
use App\Entity\Company;
use App\Entity\Member;
use App\Entity\UserIndex;
use App\Entity\CorporateMember;
use App\Entity\MemberEmail;
use App\Entity\Role;
use App\Entity\Roles;
use App\Entity\User;
use App\Entity\UserRoleAssignment;
use Doctrine\ORM\EntityManager;
use Travelodge\MS\BulkRegistrationBundle\Model\UsersUpload;
use App\Manager\EncryptionManager;
use App\Representation\CorporateMembers;
use Swift_Mailer;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Security\PasswordEncoder;

/**
 * Class BulkRegistrationService
 * @package Travelodge\MS\BulkRegistrationBundle\Services
 */
class BulkRegistrationService
{
    /** @var EntityManager $em  */
    private $em;

    /** @var TokenStorageInterface $token */
    private $token;

    /** @var \Swift_Mailer $mailer */
    private $mailer;
    
    private $templating;

    const CORP_REG_FROM = 'co';

    /** @var PasswordEncoder $passwordEncoder */
    private $passwordEncoder;

    /** @var EncryptionManager $encryptionManager*/
    private $encryptionManager;

    /**
     * BulkRegistrationService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, 
    EncryptionManager $encryptionManager,
    TokenStorageInterface $token,
    \Swift_Mailer $mailer,
    \Twig\Environment $templating,
    PasswordEncoder $passwordEncoder)
    {
        $this->em = $em;
        $this->encryptionManager = $encryptionManager;
        $this->token = $token;
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->passwordEncoder = $passwordEncoder;
    }
    
    /**
     * @param UsersUpload $users
     * @return UsersUpload
     */
    public function saveMember(UsersUpload $users)
    {
        $userId = $this->token->getToken()->getUser()->getId();

        $companyUser =  $this->em->getRepository(CorporateMember::class)->find($userId);
        $companyDetail = $this->em->getRepository(Company::class)->find($companyUser->getCompanyId());
        if ($companyDetail) {
            foreach ($users->getUsers() as $user) {
                $newUserIndex = new UserIndex();
                $this->em->persist($newUserIndex);
                $this->em->flush();
            
                $address = new Address();
                $address->setLine1($user->getAddOne());
                $address->setLine2($user->getAddTwo());
                $address->setLine3($user->getAddThree().' '.$user->getAddFour());
                $address->setPostcode($user->getPostCode());
                $address->setCountry($user->getCountry());
            

                $email = new MemberEmail();
                $email->setEmail($user->getEmail());
                $this->em->persist($email);

                $newMember = new Member();
                $newMember->setId($newUserIndex->getUserId());
                $newMember->setTitle(current($user->getTitle()));
                $newMember->setFirstName($user->getFirstName());
                $newMember->setLastName($user->getLastName());
                $newMember->setTelephone($user->getPhone());
                $newMember->setEmailAdd($email);
                $newMember->setHomeAddress($address);
                $salt = $this->encryptionManager->generateNonce();
                $password = $this->passwordEncoder->randomPassword();
                $newMember->setSalt($salt);
                $encryptedPassword = $this->encryptionManager->createPasswordHash(
                $password,
                $salt
            );
                $newMember->setPassword($encryptedPassword);
            
                $newCorporateMember = new CorporateMember();
                $newCorporateMember->setId($newUserIndex->getUserId());
                $newCorporateMember->setOperaId(time());
                $newCorporateMember->setCompanyDetail($companyDetail);
                $newCorporateMember->setRole('CorporateBooker');
                $newCorporateMember->setTitle(current($user->getTitle()));
                $newCorporateMember->setFirstName($user->getFirstName());
                $newCorporateMember->setLastName($user->getLastName());
                $newCorporateMember->setTelephone($user->getPhone());
                $newCorporateMember->setEmail($user->getEmail());
                $newCorporateMember->setAddressComp($address);
                $newCorporateMember->setSalt($salt);
                $newCorporateMember->setActive(false);
                $newCorporateMember->setRegFrom($this::CORP_REG_FROM);
                $newCorporateMember->setPassword($encryptedPassword);
            
                $newUser = new User();
                $newUser->setTitle(current($user->getTitle()));
                $newUser->setFirstName($user->getFirstName());
                $newUser->setLastName($user->getLastName());
                $newUser->setTelephone($user->getPhone());
                $newUser->setUsername($user->getEmail());
                $newUser->setUserHomeAddress($address);
                $newUser->setUserCompanyDetail($companyDetail);
                $newUser->setSalt($salt);
                $newUser->setPassword($encryptedPassword);
                $newUser->setOldUserId($newUserIndex->getUserId());

                foreach ($user->getRoles() as $role) {
                    $newRoleAssignment = new UserRoleAssignment();
                    $newRoleAssignment->setUserRole($newUser);
                    $userRole = $this->em->getRepository(Roles::class)->validateRoles($role);
                    $newRoleAssignment->setRoleCode($userRole['role']);
                    $this->em->persist($newRoleAssignment);
                }
            
                $address->addAddressCompany($newCorporateMember);
                $address->addUserHomeAddress($newUser);
                $address->addAddress($newMember);
                $this->em->persist($address);
                $this->em->persist($newMember);
                $this->em->persist($newCorporateMember);
                $this->em->persist($newUser);
                $this->em->flush();

                $message = new \Swift_Message;
                $toAddress = [$user->getEmail()];
            
                $message
                ->setSubject('test')
                ->setFrom('webmaster@travelodge.co.uk')
                ->setTo($toAddress)
                ->setBody('Password : '.$password, 'text/html');
                $this->mailer->send($message);
            }
        }

        return true;
    }
}
